var path = require("path");
var ExtractTextPlugin = require("extract-text-webpack-plugin");

function getEntrySources(sources) {
    if (process.env.NODE_ENV !== 'production') {
        sources.push('webpack-dev-server/client?http://localhost:8080');
        sources.push('webpack/hot/only-dev-server');
    }

    return sources;
}

module.exports = {
  devtool: 'eval',
  entry: {
    mainFile: getEntrySources([
      './source/js/main.js'
    ])
  },
  output: {
    path: require("path").resolve("./distribution/"),
    publicPath: '/distribution/',
    filename: 'javascripts/distribution.js'
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        loaders: ['react-hot', 'jsx', 'babel'],
        exclude: /(node_modules|bower_components)/
      },
      {
        test: /\.scss$/,
        // loaders: ['css', 'sass']
        loader: ExtractTextPlugin.extract('css!sass')
      },
      {
        test: /\.css$/,
        // loaders: ['css', 'sass']
        loader: ExtractTextPlugin.extract('css!sass')
      },
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: "url?limit=1&name=fontfaces/[name]-[hash].[ext]&mimetype=application/font-woff"
      },
      {
        test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: "file?name=fontfaces/[name]-[hash].[ext]"
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        loaders: [ 'url?limit=1&name=images/[name]-[hash].[ext]', 'img' ]
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin('stylesheets/style.css', {
      allChunks: true
    })
  ]
};
