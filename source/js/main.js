import React from 'react'
import { render } from 'react-dom'

var App = React.createClass({
    render() {
        return (
            <div>
                <b>Hello word i'm react!!!</b>
            </div>
        )
    }
})

render((
    <App />
), document.getElementById('app-container'))